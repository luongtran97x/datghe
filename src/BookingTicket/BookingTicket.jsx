import React, { Component, Fragment } from "react";
import "./BaiTapBookingTicket.css";
import ThongTinGhe from "./ThongTinGhe";
import danhSachGheData from "../../src/Data/danhSachGhe.json"
import HangGhe from "./HangGhe";
export default class BookingTicket extends Component {
  renderHangGhe=() => {
    return danhSachGheData.map((item,index) => {
      return <div key={index} >
          <HangGhe item={item} soHangGhe={index}></HangGhe>
      </div>
    }
    )
  }
  
  render() {
    return (
      <div
        className="bookingMovie"
        style={{  
          position: "fixed",
          width: "100%",
          height: "100%",
          backgroundImage: "url(./img/bookingTicket/bgmovie.jpg)",
          backgroundSize: "cover",
        }}
      >
        <div
          style={{
            backgroundColor: "rgba(0,0,0,0.7)",
            position: "fixed",
            width: "100%",
            height: "100%",
          }}
        ></div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-8 text-center">
              <h1 className="text-warning display-4">
                ĐẶT VÉ XEM PHIM CYBERLEARN.VN
              </h1>
              <div className="mt-2 text-light" style={{ fontSize: "25px" }}>
                Màn hình
              </div>
              <div
                className="mt-2"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                }}
              >
                <div className="screen"></div>


                {this.renderHangGhe()}
              </div>
            </div>
            <div className="col-4 ">
            <div className="text-light mt-2" style={{fontSize:"35px"}}>
                DANH SÁCH GHẾ BẠN CHỌN 
              </div>
              <ThongTinGhe></ThongTinGhe>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
