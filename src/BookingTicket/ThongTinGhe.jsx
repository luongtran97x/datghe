import React, { Component } from "react";
import { connect } from "react-redux";
import { huyGheAction } from "../redux/action/actionBooking";

class ThongTinGhe extends Component {
  tongTien = () => {
    {
      this.props.danhSachGheDangDat.reduce((tongTien, item) => {
        return (tongTien += item.gia);
      }, 0);
    }
  };

  render() {
    return (
      <>
        <div>
          <button className="gheDuocChon"></button>{" "}
          <span className="text-light h4">ghế đã đặt</span>
          <br></br>
          <button className="gheDangChon my-2"></button>{" "}
          <span className="text-light h4">ghế đang đặt</span>
          <br />
          <button className="ghe ml-0"></button>{" "}
          <span className="text-light h4">ghế chưa đặt</span>
        </div>

        <div className="mt-5">
          <table className="table" border={2}>
            <thead>
              <tr className="text-light" style={{ fontSize: "20px" }}>
                <th>Số ghế</th>
                <th>Giá</th>
                <th>Hủy</th>
              </tr>
            </thead>
            <tbody>
              {this.props.danhSachGheDangDat.map((item, index) => {
                return (
                  <tr className="text-light" key={index}>
                    <td>{item.soGhe}</td>
                    <td>{item.gia.toLocaleString()}đ</td>
                    <td>
                      <button
                        className="btn btn-danger"
                        onClick={() => {
                          this.props.huyGhe(item);
                        }}
                      >
                        Hủy
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot>
              <tr className="text-light">
                <td>Tổng Tiền</td>
                <td colSpan={2} className="text-center">
                  {this.props.danhSachGheDangDat.reduce((tongTien, item) => {
                    return (tongTien += item.gia);
                  }, 0).toLocaleString()}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BookingTicketReducer.danhSachGheDangDat,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    huyGhe: (item) => {
      dispatch(huyGheAction(item));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinGhe);
