import React, { Component } from "react";
import { connect } from "react-redux";
import { datGheAction } from "../redux/action/actionBooking";

class HangGhe extends Component {
  renderGhe = () => {
    return this.props.item.danhSachGhe.map((item, index) => {
      let cssGheDaDat = "";
      let disabled = false;
      if (item.daDat) {
        cssGheDaDat = "gheDuocChon";
        disabled = true;
      }
      let cssGheDangDat = "";
      // xét trạng thái ghế đang đặt
      let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe == item.soGhe
      );
      if (indexGheDangDat !== -1) {
        cssGheDangDat = "gheDangChon";
      }
      return (
       
        <button
          onClick={() => {
            this.props.datGhe(item);
          }}
          disabled={disabled}
          className={`ghe ${cssGheDaDat} ${cssGheDangDat}`}
          key={index}
        >
          {item.soGhe}
        </button>
      );
    });
  };

  renderSoHang = () => {
    return this.props.item.danhSachGhe.map((item, index) => {
      return <button className="rowNumber">{item.soGhe}</button>;
    });
  };

  renderHangGhe = () => {
    if (this.props.sohangGhe === 0) {
      return (
        <div className="ml-5">
          {this.props.item.hang} {this.renderSoHang()}
        </div>
      );
    } else {
      return (
        <div>
          {this.props.item.hang} {this.renderGhe()}
        </div>
      );
    }
  };

  render() {
    return (
      <div
        className="text-light text-left ml-5 mt-3"
        style={{ fontSize: "30px" }}
      >
        {this.renderHangGhe()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BookingTicketReducer.danhSachGheDangDat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    datGhe: (ghe) => {
      dispatch(datGheAction(ghe));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HangGhe);
