import { dat_ghe, huy_ghe } from "../constant/constantBooking"

export const datGheAction = (ghe) => {
  return{
    type:dat_ghe,
    payload:ghe,
  }
}
export const huyGheAction = (soGhe) => {
  return {
    type:huy_ghe,
    payload:soGhe
  }
}
