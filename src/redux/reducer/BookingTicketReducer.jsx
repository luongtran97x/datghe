import { dat_ghe, huy_ghe } from "../constant/constantBooking"

const initialState= {
  danhSachGheDangDat:[
  
  ]
}


export const BookingTicketReducer = (state=initialState,action) => {
  switch(action.type){
    case dat_ghe:{
      console.log(action);
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat]
      let index = cloneDanhSachGheDangDat.findIndex((gheDangDat)=>gheDangDat.soGhe === action.payload.soGhe)
      if (index !== -1){
        cloneDanhSachGheDangDat.splice(index,1)
      }else{
        cloneDanhSachGheDangDat.push(action.payload)
      }
        return {...state,danhSachGheDangDat:cloneDanhSachGheDangDat}
    }
    case huy_ghe:{
      let huyGhe =  state.danhSachGheDangDat.filter((item)=>item.soGhe !== action.payload.soGhe)
      console.log("🚀 ~ huyGhe:", huyGhe)
      return {...state,danhSachGheDangDat:huyGhe}
    }
    default:
        return {...state}
    
  }

}
